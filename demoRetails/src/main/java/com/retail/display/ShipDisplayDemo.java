package com.retail.display;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.retail.core.Item;
import com.retail.core.Item.ShipMethod;

public class ShipDisplayDemo {

	private static final DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	public static void main(String[] args) {
	
		float totalShippingCost = 0;
	    double shippingCost;
	    ShipContext context;
		
		Date date = new Date();
	    
		List<Item> list = new ArrayList<Item>();
		list.add(new Item("567321101987","CD-Pink Floyd, Dark side of the moon" , 19.99, 0.58, ShipMethod.GROUND));
		list.add(new Item("567321101986", "CD-Beatles, Abbey Road", 17.99, 0.61, ShipMethod.GROUND));
		list.add(new Item("567321101985","CD-Queen, A Night at the Opera" , 20.49, 0.55, ShipMethod.AIR));
		list.add(new Item("567321101984","CD-Michael Jackson, Thriller" , 23.88, 0.50, ShipMethod.GROUND));
		list.add(new Item("467321101899","iPhone- WaterProof case", 9.75, 0.73, ShipMethod.AIR));
		list.add(new Item("477321101878","iPhones-Headphones" , 17.25, 3.21,ShipMethod.GROUND));

		Collections.sort(list, new SortUpc());
System.out.println("****SHIPMENT REPORT*****                                     " + sdf.format(date)+ '\n');
		
		System.out.println("UPC               " + "Description              " + '\t'+
		"Price   " +  "Weight   " + "ShipMethod      "+ "Shipping Cost");	
		
		for(Item i:list) {
			
			//ShipContext context = new ShipContext(new DisplayAirCost());	
			//shippingCost = context.executeShippingCost(i.getShip(), i);
			if(i.getShip().toString().equals("AIR")) {
	// Now using my ShipContext class to see change in behavior when it changes it's strategy.
				
				context = new ShipContext(new DisplayAirCost());	
				shippingCost = context.executeShippingCost(i);
			}
			else {
				context = new ShipContext(new DisplayGroundCost());
				 shippingCost = context.executeShippingCost(i);
			}
		
			 System.out.println(i.getUpc() +'\t'+ 
					 i.getDescription() + '\t'+'\t'+
					 i.getPrice() + '\t'+ 
					 i.getWeight() + '\t'+ 
					 i.getShip()+ '\t' + shippingCost);		
						
			totalShippingCost = (float) (totalShippingCost + shippingCost);
			
		}
		System.out.printf("%-91s%.2f", "TOTAL SHIPPING COST:", totalShippingCost);
	}

}
