package com.retail.display;

import java.text.DecimalFormat;

import com.retail.core.Item;

public class DisplayGroundCost implements Retail {

	Double shipCost;
	int result;
	Double shippingTerrif;
	
	public double getShippingCost(Item item) {
		 shipCost = (item.getWeight() * 2.5);
		 DecimalFormat f = new DecimalFormat("##.00");
     	return shipCost;

}

	public Double getShipCost() {
		return shipCost;
	}

	public void setShipCost(Double shipCost) {
		this.shipCost = shipCost;
	}

	public Double getShippingTerrif() {
		return shippingTerrif;
	}

	public void setShippingTerrif(Double shippingTerrif) {
		this.shippingTerrif = shippingTerrif;
	}
	
	}