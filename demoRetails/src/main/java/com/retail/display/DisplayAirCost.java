package com.retail.display;

import java.text.DecimalFormat;
import com.retail.core.Item;

public class DisplayAirCost implements Retail {

	Double shipCost;
	int result;
	Double shippingTerrif;
	
	public double getShippingCost(Item item) {
		String str = item.getUpc();
		
		int length = str.length();
		
		if(length >=2) {
			result = Character.getNumericValue(str.charAt(str.length()-2));
		}
		shipCost = (item.getWeight() * result);	
		
		 DecimalFormat f = new DecimalFormat("##.00");
	    	
		return shipCost;

}

	public Double getShipCost() {
		return shipCost;
	}

	public void setShipCost(Double shipCost) {
		this.shipCost = shipCost;
	}

	public Double getShippingTerrif() {
		return shippingTerrif;
	}

	public void setShippingTerrif(Double shippingTerrif) {
		this.shippingTerrif = shippingTerrif;
	}
	
	}
